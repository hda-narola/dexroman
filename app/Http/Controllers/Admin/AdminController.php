<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Validator;

class AdminController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile() {
        return view('admin.profile');
    }

    public function changePassword() {
        return view('admin.auth.change_password');
    }

    public function saveChangedPassword(Request $request) {
        try{
            $validator = Validator::make($request->all(), [
                        'current_password' => 'required',
                        'new_password' => 'required|string|min:8',
                        'repeat_password' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                return redirect('password/change')->withErrors($validator)->withInput();
            }

            $current_password = Auth::User()->password;

            if (\Hash::check($request->input('current_password'), $current_password)) {
                $user_id = Auth::User()->id;
                $obj_user = User::find($user_id);
                $obj_user->password = \Hash::make($request->input('new_password'));
                $isChanged = $obj_user->save();
                
                if($isChanged){
                    return redirect('password/change')->with('success','Your password has been changed successfully.');
                }
            }else{
                return redirect('password/change')->with('error','Your current password do not match with our records.');
            }
        } catch (\Exception $e){
            return redirect('password/change')->with('error','Somthing problem with password updation.');
        }
    }

}
