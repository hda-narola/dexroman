<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Admin\HomeController@index');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'Admin\HomeController@index')->name('dashboard');
Route::get('/profile/edit', 'Admin\AdminController@profile')->name('profile.edit');
Route::get('/password/change', 'Admin\AdminController@changePassword')->name('password.change');
Route::post('/password/change/save', 'Admin\AdminController@saveChangedPassword')->name('save.changed.password');
