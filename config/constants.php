<?php

/**
 * Constant file for global constants
 * @author KU
 */

define('PROFILE_IMAGES', 'https://static.wywrota.pl/thumb/?src=/pliki/site_images/');

return [
    'APP_NAME' => 'Dexroman',
    'APP_LOGO' => 'Dexroman',
    'APP_FAVICON' => '//d85wutc1n854v.cloudfront.net/live/imgs/favicon.ico?v=1',
];