@extends('emails.layouts.app')

@section('extra_styles')
@endsection

@section('content')
<tr>
    <td>
        <!-- begin row -->
        <table class="row">
            <tr>
                <!-- begin wrapper -->
                <td class="wrapper">
                    <table class="twelve columns">
                        <tr>
                            <td class="last">
                                <h4>Welcome to Wrapbootstrap.</h4>
                                <p class="m-b-5">Please click the following URL to activate your account:</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="panel">
                                <a href="javascript:;">https://www.wrapbootstrap.com/registration/activate/?code=28a782891</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p class="m-t-15 last">If clicking the URL above does not work, copy and paste the URL into a browser window.</p>
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- end wrapper -->
            </tr>
        </table>
        <!-- end row -->
        <!-- begin divider -->
        <table class="divider"></table>
        <!-- end divider -->
        <!-- begin row -->
        <table class="row">
            <tr>
                <!-- begin wrapper -->
                <td class="wrapper">
                    <!-- begin twelve columns -->
                    <table class="twelve columns">
                        <tr>
                            <td>
                                <h4>Discount 80%!</h4>
                                <p>
                                    Quisque eget pulvinar nisi, et mattis leo. Aenean nec ligula aliquet, tincidunt massa ac, eleifend velit. Ut ac enim a enim tempor volutpat. Mauris quis sagittis nunc, et convallis nulla. Fusce gravida lacus sed vulputate scelerisque.
                                </p>
                                <p>
                                    <a href="index.html"><img src="../assets/img/product/product-preview.jpg" /></a>
                                </p>
                            </td>
                        </tr>
                    </table>
                    <!-- end twelve columns -->
                    <p>
                        <i>This ia a system generated email and reply is not required.</i>
                    </p>
                </td>
                <!-- end wrapper -->
            </tr>
        </table>
        <!-- end row -->
    </td>
</tr>
@endsection