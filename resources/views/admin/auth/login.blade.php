@extends('admin.layouts.auth_app')

@section('title', 'Login')

@section('content')
<!-- begin brand -->
<div class="login-header">
    <div class="brand">
        <span class="logo"></span> <b>{{ strtoupper(config('constants.APP_NAME')) }}</b> Admin
        <small>Sign in to access.</small>
    </div>
    <div class="icon">
        <i class="fa fa-lock"></i>
    </div>
</div>
<!-- end brand -->
<!-- begin login-content -->
<div class="login-content">
    <form action="{{ route('login') }}" method="POST" class="margin-bottom-0">
        @csrf
        <div class="form-group m-b-20">
            <input id="email" type="email" placeholder="Email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group m-b-20">
            <input id="password" type="password" placeholder="Password" class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
        <div class="checkbox checkbox-css m-b-20">
            <input type="checkbox" id="remember_checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                   <label for="remember_checkbox">
                Remember Me
            </label>
        </div>
        <div class="login-buttons">
            <button type="submit" class="btn btn-success btn-block btn-lg">Sign in</button>
        </div>
        <div class="m-t-20">
            Forgot Password? Click <a href="{{ route('password.request') }}">Here</a>.
        </div>
    </form>
</div>
<!-- end login-content -->

@endsection

@section('extra_scripts')
@endsection