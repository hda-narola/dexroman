@extends('admin.layouts.auth_app')

@section('title', 'Reset Password')

@section('content')
<!-- begin brand -->
<div class="login-header">
    <div class="brand">
        <span class="logo"></span> <b>{{ strtoupper(config('constants.APP_NAME')) }}</b> Admin
        <small>Reset Your Password.</small>
    </div>
    <div class="icon">
        <i class="fa fa-key"></i>
    </div>
</div>
<!-- end brand -->
<!-- begin login-content -->
<div class="login-content">
    <form method="POST" action="{{ route('password.request') }}" class="margin-bottom-0">
        @csrf
        
        <input type="hidden" name="token" value="{{ $token }}">
        
        <div class="form-group m-b-20">
            <input id="email" type="email" placeholder="Email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group m-b-20">
            <input id="password" type="password" placeholder="Password" class="form-control form-control-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group m-b-20">
            <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control form-control-lg" name="password_confirmation" required>
        </div>
        <div class="login-buttons">
            <button type="submit" class="btn btn-success btn-block btn-lg">Reset Password</button>
        </div>
    </form>
</div>
<!-- end login-content -->

@endsection

@section('extra_scripts')
@endsection