@extends('admin.layouts.auth_app')

@section('title', 'Forgot Password')

@section('content')
<!-- begin brand -->
<div class="login-header">
    <div class="brand">
        <span class="logo"></span> <b>{{ strtoupper(config('constants.APP_NAME')) }}</b> Admin
        <small>Forgot Password</small>
    </div>
    <div class="icon">
        <i class="fa fa-lock"></i>
    </div>
</div>
<!-- end brand -->
<!-- begin login-content -->
<div class="login-content">
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif
    <form method="POST" action="{{ route('password.email') }}" class="margin-bottom-0">
        @csrf
        <div class="form-group m-b-20">
            <input id="email" type="email" placeholder="Email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>        
        <div class="login-buttons">
            <button type="submit" class="btn btn-success btn-block btn-lg">Send Password Reset Link</button>
        </div>
        <div class="m-t-20">
            <a href="{{ route('login') }}">Sign in</a>.
        </div>
    </form>
</div>
<!-- end login-content -->
@endsection

@section('extra_scripts')
@endsection