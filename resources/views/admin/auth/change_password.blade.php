@extends('admin.layouts.app')

@section('title', 'Change Password')

@section('header_navbars')
@parent
@endsection

@section('sidebar')
@parent
@endsection

@section('content')

<!-- begin breadcrumb -->
<div class="pull-right">{{ Breadcrumbs::render('change_password') }}</div>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Change Password <small>header small text goes here...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Change Password</h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ route('save.changed.password') }}" method="POST" data-parsley-validate="true" name="demo-form">
            @csrf
            
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="current_password">Current Password * :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" type="password" id="current_password" name="current_password" placeholder="Current Password" data-parsley-required="true" />

                    @if ($errors->has('current_password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('current_password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="new_password">New Password * :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" 
                           type="password" 
                           id="new_password" 
                           name="new_password" 
                           placeholder="New Password" 
                           data-parsley-required 
                           data-parsley-minlength="8"
                           data-parsley-required-message="Please enter your new password."
                           data-parsley-uppercase="1"
                           data-parsley-lowercase="1"
                           data-parsley-number="1"
                           data-parsley-special="1" />

                    @if ($errors->has('new_password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('new_password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="repeat_password">Repeat New Password * :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" 
                           type="password" 
                           id="repeat_password" 
                           name="repeat_password" 
                           placeholder="Repeat Password" 
                           data-parsley-required
                           data-parsley-minlength="8"
                           data-parsley-required-message="Please re-enter your repeat password."
                           data-parsley-equalto="#new_password" />

                    @if ($errors->has('repeat_password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('repeat_password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row m-b-0">
                <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                <div class="col-md-8 col-sm-8">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end panel -->
@endsection