<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>@yield('title') - {{ config('constants.APP_NAME') }}</title>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
        <meta content="{{ config('constants.APP_NAME') }}" name="description" />
        <meta content="{{ config('constants.APP_NAME') }}" name="author" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ config('constants.APP_FAVICON') }}">

        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/bootstrap/4.1.0/css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/font-awesome/5.0/css/fontawesome-all.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/animate/animate.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/default/style.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/default/style-responsive.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/css/default/theme/default.css') }}" rel="stylesheet" id="theme" />
        <!-- ================== END BASE CSS STYLE ================== -->

        <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
        <link href="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
        <!-- ================== END PAGE LEVEL STYLE ================== -->

        <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
        <link href="{{ asset('assets/plugins/parsley/src/parsley.css') }}" rel="stylesheet" />
        <!-- ================== END PAGE LEVEL STYLE ================== -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
        <!-- ================== END BASE JS ================== -->

        <!--Add extra stylesheet from child blades-->
        @section('extra_styles')        
        @show

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
        <!-- ================== END BASE JS ================== -->
    </head>
    <body>
        <!-- begin #page-loader -->
        <div id="page-loader" class="fade show">
            <span class="spinner"></span>
        </div>
        <!-- end #page-loader -->

        <!-- begin #page-container -->
        <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
            <!-- begin #header -->
            @section('header_navbars')
            <div id="header" class="header navbar-default">
                <!-- begin navbar-header -->
                <div class="navbar-header">
                    <a href="{{ url('/dashboard') }}" class="navbar-brand"><span class="navbar-logo"></span> 
                        <b>{{ strtoupper(config('constants.APP_NAME')) }}</b> Admin
                    </a>
                    <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end navbar-header -->

                <!-- begin header-nav -->
                <ul class="navbar-nav navbar-right">
                    <li>
                        <form class="navbar-form">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Enter keyword" />
                                <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                            </div>
                        </form>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle f-s-14">
                            <i class="fa fa-bell"></i>
                            <span class="label">5</span>
                        </a>
                        <ul class="dropdown-menu media-list dropdown-menu-right">
                            <li class="dropdown-header">NOTIFICATIONS (5)</li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left">
                                        <i class="fa fa-bug media-object bg-silver-darker"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Server Error Reports <i class="fa fa-exclamation-circle text-danger"></i></h6>
                                        <div class="text-muted f-s-11">3 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left">
                                        <img src="{{ asset('assets/img/user/user-1.jpg') }}" class="media-object" alt="" />
                                        <i class="fab fa-facebook-messenger text-primary media-object-icon"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">John Smith</h6>
                                        <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                        <div class="text-muted f-s-11">25 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left">
                                        <img src="{{ asset('assets/img/user/user-2.jpg') }}" class="media-object" alt="" />
                                        <i class="fab fa-facebook-messenger text-primary media-object-icon"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading">Olivia</h6>
                                        <p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
                                        <div class="text-muted f-s-11">35 minutes ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left">
                                        <i class="fa fa-plus media-object bg-silver-darker"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> New User Registered</h6>
                                        <div class="text-muted f-s-11">1 hour ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="media">
                                <a href="javascript:;">
                                    <div class="media-left">
                                        <i class="fa fa-envelope media-object bg-silver-darker"></i>
                                        <i class="fab fa-google text-warning media-object-icon f-s-14"></i>
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading"> New Email From John</h6>
                                        <div class="text-muted f-s-11">2 hour ago</div>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-footer text-center">
                                <a href="javascript:;">View more</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown navbar-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('assets/img/user/user-13.jpg') }}" alt="" /> 
                            <span class="d-none d-md-inline">{{ Auth::user()->name }}</span> <b class="caret"></b>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('profile.edit') }}" class="dropdown-item">Edit Profile</a>
                            <a href="{{ route('password.change') }}" class="dropdown-item">Change Password</a>
                            <a href="javascript:;" class="dropdown-item"><span class="badge badge-danger pull-right">2</span> Inbox</a>
                            <a href="javascript:;" class="dropdown-item">Calendar</a>
                            <a href="javascript:;" class="dropdown-item">Setting</a>
                            <div class="dropdown-divider"></div>

                            <!--Logout with form-->
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hide">
                                @csrf
                            </form>
                            <!--<a href="javascript:;" class="dropdown-item">Log Out</a>-->
                        </div>
                    </li>
                </ul>
                <!-- end header navigation right -->
            </div>
            @show
            <!-- end #header -->

            <!-- begin #sidebar -->
            @section('sidebar')
            <div id="sidebar" class="sidebar">
                <!-- begin sidebar scrollbar -->
                <div data-scrollbar="true" data-height="100%">
                    <!-- begin sidebar user -->
                    <ul class="nav">
                        <li class="nav-profile">
                            <a href="javascript:;" data-toggle="nav-profile">
                                <div class="cover with-shadow"></div>
                                <div class="image">
                                    <img src="{{ asset('assets/img/user/user-13.jpg') }}" alt="" />
                                </div>
                                <div class="info">
                                    <b class="caret pull-right"></b>
                                    {{ Auth::user()->name }}
                                    <!--<small>Front end developer</small>-->
                                </div>
                            </a>
                        </li>
                        <li>
                            <ul class="nav nav-profile">
                                <li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
                                <li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li>
                                <li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- end sidebar user -->
                    <!-- begin sidebar nav -->
                    <ul class="nav">
                        <li class="nav-header">Navigation</li>
                        <li class="has-sub active">
                            <a href="javascript:;">
                                <b class="caret"></b>
                                <i class="fa fa-th-large"></i>
                                <span>Dashboard</span>
                            </a>
                            <ul class="sub-menu">
                                <li class="active"><a href="index.html">Dashboard v1</a></li>
                                <li><a href="index_v2.html">Dashboard v2</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;">
                                <b class="caret"></b>
                                <i class="fa fa-align-left"></i> 
                                <span>Menu Level</span>
                            </a>
                            <ul class="sub-menu">
                                <li class="has-sub">
                                    <a href="javascript:;">
                                        <b class="caret"></b>
                                        Menu 1.1
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="has-sub">
                                            <a href="javascript:;">
                                                <b class="caret"></b>
                                                Menu 2.1
                                            </a>
                                            <ul class="sub-menu">
                                                <li><a href="javascript:;">Menu 3.1</a></li>
                                                <li><a href="javascript:;">Menu 3.2</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:;">Menu 2.2</a></li>
                                        <li><a href="javascript:;">Menu 2.3</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:;">Menu 1.2</a></li>
                                <li><a href="javascript:;">Menu 1.3</a></li>
                            </ul>
                        </li>
                        <!-- begin sidebar minify button -->
                        <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
                        <!-- end sidebar minify button -->
                    </ul>
                    <!-- end sidebar nav -->
                </div>
                <!-- end sidebar scrollbar -->
            </div>
            <div class="sidebar-bg"></div>
            <!-- end #sidebar -->
            @show

            <!-- begin #content -->
            <div id="content" class="content">

                @if(Session::has('success'))
                <div class="alert alert-success fade show m-b-10">
                    <span class="close" data-dismiss="alert">×</span>
                    {{ Session::get('success') }}
                </div>
                @endif

                @if(Session::has('error'))
                <div class="alert alert-danger fade show m-b-10">
                    <span class="close" data-dismiss="alert">×</span>
                    {{ Session::get('error') }}
                </div>
                @endif

                @yield('content')
            </div>
            <!-- end #content -->

            <!-- begin theme-panel -->
            <div class="theme-panel theme-panel-lg hide">
                <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
                <div class="theme-panel-content">
                    <h5 class="m-t-0">Color Theme</h5>
                    <ul class="theme-list clearfix">
                        <li><a href="javascript:;" class="bg-red" data-theme="red" data-theme-file="{{ asset('assets/css/default/theme/red.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Red">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-pink" data-theme="pink" data-theme-file="{{ asset('assets/css/default/theme/pink.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Pink">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-orange" data-theme="orange" data-theme-file="{{ asset('assets/css/default/theme/orange.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Orange">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-yellow" data-theme="yellow" data-theme-file="{{ asset('assets/css/default/theme/yellow.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Yellow">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-lime" data-theme="lime" data-theme-file="{{ asset('assets/css/default/theme/lime.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Lime">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-green" data-theme="green" data-theme-file="{{ asset('assets/css/default/theme/green.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Green">&nbsp;</a></li>
                        <li class="active"><a href="javascript:;" class="bg-teal" data-theme="default" data-theme-file="{{ asset('assets/css/default/theme/default.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Default">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-aqua" data-theme="aqua" data-theme-file="{{ asset('assets/css/default/theme/aqua.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Aqua">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-blue" data-theme="blue" data-theme-file="{{ asset('assets/css/default/theme/blue.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Blue">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-purple" data-theme="purple" data-theme-file="{{ asset('assets/css/default/theme/purple.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Purple">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-indigo" data-theme="indigo" data-theme-file="{{ asset('assets/css/default/theme/indigo.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Indigo">&nbsp;</a></li>
                        <li><a href="javascript:;" class="bg-black" data-theme="black" data-theme-file="{{ asset('assets/css/default/theme/black.css') }}" data-click="theme-selector" data-toggle="tooltip" data-trigger="hover" data-container="body" data-title="Black">&nbsp;</a></li>
                    </ul>
                    <div class="divider"></div>
                    <div class="row m-t-10">
                        <div class="col-md-6 control-label text-inverse f-w-600">Header Styling</div>
                        <div class="col-md-6">
                            <select name="header-styling" class="form-control form-control-sm">
                                <option value="1">default</option>
                                <option value="2">inverse</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-6 control-label text-inverse f-w-600">Header</div>
                        <div class="col-md-6">
                            <select name="header-fixed" class="form-control form-control-sm">
                                <option value="1">fixed</option>
                                <option value="2">default</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-6 control-label text-inverse f-w-600">Sidebar Styling</div>
                        <div class="col-md-6">
                            <select name="sidebar-styling" class="form-control form-control-sm">
                                <option value="1">default</option>
                                <option value="2">grid</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-6 control-label text-inverse f-w-600">Sidebar</div>
                        <div class="col-md-6">
                            <select name="sidebar-fixed" class="form-control form-control-sm">
                                <option value="1">fixed</option>
                                <option value="2">default</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-6 control-label text-inverse f-w-600">Sidebar Gradient</div>
                        <div class="col-md-6">
                            <select name="content-gradient" class="form-control form-control-sm">
                                <option value="1">disabled</option>
                                <option value="2">enabled</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-6 control-label text-inverse f-w-600">Content Styling</div>
                        <div class="col-md-6">
                            <select name="content-styling" class="form-control form-control-sm">
                                <option value="1">default</option>
                                <option value="2">black</option>
                            </select>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-md-6 control-label text-inverse f-w-600">Direction</div>
                        <div class="col-md-6">
                            <select name="direction" class="form-control form-control-sm">
                                <option value="1">LTR</option>
                                <option value="2">RTL</option>
                            </select>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <h5>THEME VERSION</h5>
                    <div class="theme-version">
                        <a href="../template_html/index_v2.html" class="active">
                            <span style="background-image: url(../assets/img/theme/default.jpg);"></span>
                        </a>
                        <a href="../template_transparent/index_v2.html">
                            <span style="background-image: url(../assets/img/theme/transparent.jpg);"></span>
                        </a>
                    </div>
                    <div class="theme-version">
                        <a href="../template_apple/index_v2.html">
                            <span style="background-image: url(../assets/img/theme/apple.jpg);"></span>
                        </a>
                        <a href="../template_material/index_v2.html">
                            <span style="background-image: url(../assets/img/theme/material.jpg);"></span>
                        </a>
                    </div>
                    <div class="divider"></div>
                    <div class="row m-t-10">
                        <div class="col-md-12">
                            <a href="javascript:;" class="btn btn-inverse btn-block btn-rounded" data-click="reset-local-storage"><b>Reset Local Storage</b></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end theme-panel -->

            <!-- begin scroll to top btn -->
            <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
            <!-- end scroll to top btn -->
        </div>
        <!-- end page container -->

        <!-- ================== BEGIN BASE JS ================== -->
        <script src="{{ asset('assets/plugins/jquery/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap/4.1.0/js/bootstrap.bundle.min.js') }}"></script>
        <!--[if lt IE 9]>
                <script src="{{ asset('assets/crossbrowserjs/html5shiv.js') }}"></script>
                <script src="{{ asset('assets/crossbrowserjs/respond.min.js') }}"></script>
                <script src="{{ asset('assets/crossbrowserjs/excanvas.min.js') }}"></script>
        <![endif]-->
        <script src="{{ asset('assets/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/js-cookie/js.cookie.js') }}"></script>
        <script src="{{ asset('assets/js/theme/default.min.js') }}"></script>
        <script src="{{ asset('assets/js/apps.min.js') }}"></script>
        <!-- ================== END BASE JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="{{ asset('assets/plugins/gritter/js/jquery.gritter.js') }}"></script>
        <script src="{{ asset('assets/plugins/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/flot/jquery.flot.time.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/flot/jquery.flot.resize.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/flot/jquery.flot.pie.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/sparkline/jquery.sparkline.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('assets/js/demo/dashboard.min.js') }}"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->

        <!-- ================== BEGIN PAGE LEVEL JS ================== -->
        <script src="{{ asset('assets/plugins/parsley/dist/parsley.js') }}"></script>
        <script src="{{ asset('assets/plugins/highlight/highlight.common.js') }}"></script>
        <script src="{{ asset('assets/js/demo/render.highlight.js') }}"></script>
        <!-- ================== END PAGE LEVEL JS ================== -->

        <script>
            $(document).ready(function () {
                App.init();

                // Welcome message popup on right-bottom side                 
                handleDashboardGritterNotification = function () {
                    $(window).on("load", function () {
                        setTimeout(function () {
                            $.gritter.add({
                                title: "Welcome back, {{ Auth::user()->name }}!",
                                text: "",
                                image: "{{ asset('/assets/img/user/user-12.jpg') }}",
                                sticky: !0,
                                time: "",
                                class_name: "my-sticky-class"
                            })
                        }, 1e3)
                    })
                };
                Dashboard.init();
                Highlight.init();
            });
        </script>

        @section('extra_scripts')
        @show
    </body>
</html>