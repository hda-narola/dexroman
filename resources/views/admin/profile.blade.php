@extends('admin.layouts.app')

@section('title', 'Edit Profile')

@section('header_navbars')
@parent
@endsection

@section('sidebar')
@parent
@endsection

@section('content')

<!-- begin breadcrumb -->
<div class="pull-right">{{ Breadcrumbs::render('edit_profile') }}</div>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Profile <small>header small text goes here...</small></h1>
<!-- end page-header -->

<!-- begin panel -->
<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Edit Profile</h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" data-parsley-validate="true" name="demo-form">
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="fullname">Full Name * :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" type="text" id="fullname" name="fullname" placeholder="Required" data-parsley-required="true" />
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="email">Email * :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" type="text" id="email" name="email" data-parsley-type="email" placeholder="Email" data-parsley-required="true" />
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="website">Website :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" type="url" id="website" name="website" data-parsley-type="url" placeholder="url" data-parsley-required="true" />
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="message">Message (20 chars min, 200 max) :</label>
                <div class="col-md-8 col-sm-8">
                    <textarea class="form-control" id="message" name="message" rows="4" data-parsley-range="[20,200]" placeholder="Range from 20 - 200"></textarea>
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="message">Digits :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" type="text" id="digits" name="digits" data-parsley-type="digits" placeholder="Digits" />
                </div>
            </div>
            <div class="form-group row m-b-15">
                <label class="col-md-4 col-sm-4 col-form-label" for="message">Number :</label>
                <div class="col-md-8 col-sm-8">
                    <input class="form-control" type="text" id="number" name="number" data-parsley-type="number" placeholder="Number" />
                </div>
            </div>
            <div class="form-group row m-b-0">
                <label class="col-md-4 col-sm-4 col-form-label">&nbsp;</label>
                <div class="col-md-8 col-sm-8">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end panel -->
@endsection

@section('extra_scripts')
<script>
    $(document).ready(function () {
        Highlight.init();
    });
</script>
@endsection